namespace TheOfficeCalculator;

public class Display
{
    private readonly string _helloWorld;

    public Display(string helloWorld)
    {
        _helloWorld = helloWorld;
    }

    public IEnumerable<char> WelcomeMessage()
    {
        return _helloWorld;
    }
}