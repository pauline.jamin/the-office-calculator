﻿// See https://aka.ms/new-console-template for more information

using TheOfficeCalculator;

var display = new Display("Hello, Kurt!");
Console.WriteLine(display.WelcomeMessage());

Console.WriteLine("Enter ream price:");
var value = Console.ReadLine();

if (int.TryParse(value, out var price))
{
    var result = Calculator.Calculate(price, 1);
    Console.WriteLine($"The ream price is: {result}");
}
else
{
    Console.WriteLine("Ream price must be a positive integer");
}

Console.WriteLine("Enter ream quantity:");
var quantity = Console.ReadLine();

if (int.TryParse(quantity, out var reamCount))
{
    var result = Calculator.Calculate(price, reamCount);
    Console.WriteLine($"The tax-free result is: {result}");
}
else
{
    Console.WriteLine("Ream quantity must be a positive integer");
}

Console.WriteLine("Press any key to leave...");

_ = Console.ReadLine();
