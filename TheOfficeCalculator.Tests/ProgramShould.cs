using Xunit;

namespace TheOfficeCalculator.Tests;

public class ProgramShould
{
    [Fact]
    public void ReturnWelcomeToUser()
    {
        // Arrange
        const string helloWorld = "Hello world!";
        var display = new Display(helloWorld);

        // Act
        var result = display.WelcomeMessage();

        // Assert
        Assert.Equal(result, helloWorld);
    }

    [Theory]
    [InlineData(1)]
    public void ReturnPriceOfReam(int value)
    {
        // Arrange
        var reamPrice = value;

        // Act
        var result = Calculator.Calculate(reamPrice, 1);

        // Assert
        Assert.Equal(result, value);
    }

    [Fact]
    public void ReturnPriceOfMultipleReams()
    {
        // Arrange
        const int reamPrice = 5;
        const int reamCount = 10;

        // Act
        var result = Calculator.Calculate(reamPrice, reamCount);

        // Assert
        Assert.Equal(result, 50);
    }

    [Fact]
    public void ReturnPriceOfMultipleReamsWithTax()
    {
        // Arrange
        const int reamPrice = 5;
        const int reamCount = 10;

        // Act
        var result = Calculator.Calculate(reamPrice, reamCount);

        // Assert
        Assert.Equal(result, 50);
    }
}